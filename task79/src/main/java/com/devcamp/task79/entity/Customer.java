package com.devcamp.task79.entity;

import javax.persistence.*;

@Entity
@Table(name = "customers")
public class Customer {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "address")
   private String address;

   @Column(name = "city")
   private String city;

   @Column(name = "country")
   private String country;

   @Column(name = "credit_limit")
   private int creditLimit;

   @Column(name = "first_name")
   private String firstName;

   @Column(name = "last_name")
   private String lastName;

   @Column(name = "phone_number")
   private String phoneNumber;

   @Column(name = "postal_code")
   private String postalCode;

   @Column(name = "sales_rep_employee_number")
   private int salesRepEmployeeNumber;

   private String state;

   public Customer() {
   }

   public Customer(int id, String address, String city, String country, int creditLimit, String firstName,
         String lastName, String phoneNumber, String postalCode, int salesRepEmployeeNumber, String state) {
      this.id = id;
      this.address = address;
      this.city = city;
      this.country = country;
      this.creditLimit = creditLimit;
      this.firstName = firstName;
      this.lastName = lastName;
      this.phoneNumber = phoneNumber;
      this.postalCode = postalCode;
      this.salesRepEmployeeNumber = salesRepEmployeeNumber;
      this.state = state;
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getAddress() {
      return this.address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getCity() {
      return this.city;
   }

   public void setCity(String city) {
      this.city = city;
   }

   public String getCountry() {
      return this.country;
   }

   public void setCountry(String country) {
      this.country = country;
   }

   public int getCreditLimit() {
      return this.creditLimit;
   }

   public void setCreditLimit(int creditLimit) {
      this.creditLimit = creditLimit;
   }

   public String getFirstName() {
      return this.firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getLastName() {
      return this.lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getPhoneNumber() {
      return this.phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }

   public String getPostalCode() {
      return this.postalCode;
   }

   public void setPostalCode(String postalCode) {
      this.postalCode = postalCode;
   }

   public int getSalesRepEmployeeNumber() {
      return this.salesRepEmployeeNumber;
   }

   public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
      this.salesRepEmployeeNumber = salesRepEmployeeNumber;
   }

   public String getState() {
      return this.state;
   }

   public void setState(String state) {
      this.state = state;
   }

}
