package com.devcamp.task79.entity;

public interface INumberOfCustomerGroupByCountry {
   Integer getNumberOfCustomer();

   String getCountry();
}
