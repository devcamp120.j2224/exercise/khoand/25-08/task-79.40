package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.task79.entity.Customer;
import com.devcamp.task79.entity.INumberOfCustomerGroupByCountry;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {
   @Query(value = "SELECT COUNT(country) AS numberOfCustomer, country FROM customers WHERE country = 'USA' OR country = 'France' OR country = 'Singapore' OR country = 'Spain' GROUP BY country", nativeQuery = true)
   List<INumberOfCustomerGroupByCountry> getNumberOfCustomerGroupByCountryName();
}
