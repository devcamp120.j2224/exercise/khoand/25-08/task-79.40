package com.devcamp.task79.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task79.entity.Customer;
import com.devcamp.task79.entity.INumberOfCustomerGroupByCountry;
import com.devcamp.task79.repository.ICustomerRepository;
import com.devcamp.task79.service.ExcelExporter;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
   @Autowired
   private ICustomerRepository customerRepository;

   @GetMapping("/customers")
   public ResponseEntity<List<Customer>> getAllCustomerWithFullName() {
      return new ResponseEntity<>(customerRepository.findAll(), HttpStatus.OK);
   }

   @GetMapping("/customers/countries")
   public ResponseEntity<List<INumberOfCustomerGroupByCountry>> getNumberOfCustomerGroupByCountryName() {
      return new ResponseEntity<>(customerRepository.getNumberOfCustomerGroupByCountryName(), HttpStatus.OK);
   }

   @GetMapping("/export/customers/excel")
   public void exportToExcel(HttpServletResponse response) throws IOException {
      response.setContentType("application/octet-stream");
      DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
      String currentDateTime = dateFormatter.format(new Date());

      String headerKey = "Content-Disposition";
      String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
      response.setHeader(headerKey, headerValue);

      List<Customer> customerList = new ArrayList<Customer>();

      customerRepository.findAll().forEach(customerList::add);

      ExcelExporter excelExporter = new ExcelExporter(customerList);

      excelExporter.export(response);
   }
}
